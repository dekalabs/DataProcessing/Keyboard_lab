/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

#include "gpio.h"
#include "usart.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>

#include "keyboard.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

const uint32_t LONG_PRESS_TIME = 500;   /// How long a button must be pressed
                                        /// to use FAST mode (ms)
const uint32_t SLOW_UPDATE_TIME = 200;  /// Update interval in slow mode (ms)
const uint32_t FAST_UPDATE_TIME = 50;   /// Update interval in fast mode (ms)
const uint8_t BITMASKS[16] = {
    0b00000111, 0b00001011, 0b00010011, 0b00100011,   /// 1 col mask right turn
    0b01000011, 0b00010101, 0b00101001, 0b01001001,   /// 2 col mask right turn
    0b00000111, 0b00001011, 0b00010011, 0b00100011,   /// 3 col mask left turn
    0b01000011, 0b00010101, 0b00101001, 0b01001001};  /// 4 col mask left turn

int g_key_log_id = -1;  /// On each iteration the key will be logged
int g_mode = 0;         /// Current mode of running
uint32_t g_update_interval = SLOW_UPDATE_TIME;  /// Interval of update (ms)

/* USER CODE END PV */

/* Private function prototypes
   -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void led_clear() { HAL_GPIO_WritePin(GPIOE, 0b11111111 << 8, GPIO_PIN_RESET); }

void led_rotate_right() {
  uint8_t mask = (GPIOE->ODR & (0b11111111 << 8)) >> 8;
  mask = (mask << 1) | (mask >> 7);
  led_clear();
  HAL_GPIO_WritePin(GPIOE, mask << 8, GPIO_PIN_SET);
}
void led_rotate_left() {
  uint8_t mask = (GPIOE->ODR & (0b11111111 << 8)) >> 8;
  mask = (mask >> 1) | (mask << 7);
  led_clear();
  HAL_GPIO_WritePin(GPIOE, mask << 8, GPIO_PIN_SET);
}

void mode_start(int mode) {
  led_clear();
  uint16_t mask = BITMASKS[mode];
  HAL_GPIO_WritePin(GPIOE, mask << 8, GPIO_PIN_SET);
}

void HandleKeyboardEvent(KeyEvent ev) {
  g_key_log_id = ev.key;
  if (ev.press_duration < LONG_PRESS_TIME) {
    g_update_interval = SLOW_UPDATE_TIME;
  } else {
    g_update_interval = FAST_UPDATE_TIME;
  }

  if (ev.key != g_mode) {
    g_mode = ev.key;
  }
}
/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick.
   */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  Keyboard_Init();
  Keyboard_SetCallback(&HandleKeyboardEvent);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  int mode = g_mode;
  uint32_t time_to_update = g_update_interval;
  mode_start(g_mode);
  while (1) {
    if (g_key_log_id >= 0) {
      int key = g_key_log_id;
      g_key_log_id = -1;  /// Allow to log next button

      printf("Key pressed: %d: C%d,L%d\n", key, key % 4, key / 4);
    }

    if (mode != g_mode) {
      mode = g_mode;
      mode_start(mode);
      time_to_update = g_update_interval;
      // printf("Mode switch: #%d; UI: %ld\n", mode, time_to_update);
    }

    if (time_to_update <= 0) {
      if (mode < 8) {
        led_rotate_right();
      } else {
        led_rotate_left();
      }
      time_to_update = g_update_interval;
    }

    time_to_update--;
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    HAL_Delay(1);
  }
  /* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
   * in the RCC_OscInitTypeDef structure.
   */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
   */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK |
                                RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1) {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line) {
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line
     number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
