/**
 * GNUC specific stdout/stderr hooks.
 *
 * @note: other compilers have other stdout/stderr hooks.
 */
#ifndef __GNUC__
#error Unknown compilator
#else
#include <stm32f3xx_hal.h>
#include <unistd.h>

extern UART_HandleTypeDef huart2;

int _write(int fd, const void *buf, size_t count) {
  int res = 0;
  if (fd == STDOUT_FILENO || fd == STDERR_FILENO) {
    // write data to UART
    HAL_StatusTypeDef hal_res =
        HAL_UART_Transmit(&huart2, (uint8_t *)buf, count, HAL_MAX_DELAY);
    res = hal_res == HAL_OK ? count : -1;
  } else {
    res = -1;
  }
  return res;
}
#endif